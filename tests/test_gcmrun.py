import sys
import shutil
import os
from .util import get_test_ensembles, pause_for_file


sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', 'CCCma_tools', 'ensemble'))
from canesm import CanESMensemble


def check_gcmrun_shas(ensemble_info):

    ens = ensemble_info.ensemble
    sha = ensemble_info.sha['gcmrun']['shas']
    sha_deltas = []

    # pause until run is finished
    job = ens.jobs[0]
    runpath = job.runpath
    for s in sha:
        file = s['file'].replace('{runid}', job.runid)
        gcm_folder = os.path.join(runpath, os.path.split(file)[0])
        gcm_file = os.path.split(file)[1]
        pause_for_file(job, gcm_file, directory=gcm_folder)
        remote_sha = job.run_command(f'sha1sum {gcm_file}', setup_env=False, run_directory=gcm_folder)
        remote_sha = remote_sha.stdout.strip().split()[0]
        if not s['sha'] == remote_sha:
            # store information about checksum change
            delta_inf = {
                    'file'      : file,
                    'old_sha'   : s['sha'],
                    'new_sha'   : remote_sha
                }
            sha_deltas.append(delta_inf)

    # if deltas were noted, print info and throw exception
    if sha_deltas:
        print("***********************************")
        print("     Checksum changes noted!       ")
        print("***********************************")
        for delta in sha_deltas:
            print("\tFile: {} \n\tOld Checksum: {} \n\t New Checksum: {}\n".format(
                            delta['file'],delta['old_sha'],delta['new_sha']))
        raise Exception("Changes in the gcmrun files!")

def check_gcmrun_files(ensemble_info):

    ens = ensemble_info.ensemble
    sha = ensemble_info.sha['gcmrun']['files']

    # pause until run is finished
    job = ens.jobs[0]
    queue_dir = f'/home/{job.user}/.queue'
    stored_output_dir = os.path.join(queue_dir,".ci_failures")

    for s in sha:
        file = s.replace('{runid}', job.runid)
        file = pause_for_file(job, file, directory=queue_dir)
        for f in file:
            contents = job.load_file(f, directory=queue_dir)
            if not 'Job Successful' in contents:
                # store information on which stage failed and store output file
                shutil.move(os.path.join(queue_dir,f),os.path.join(stored_output_dir,f))

                # output failure information
                print("*****************************************")
                print("     GCMRUN failed!                      ")
                print("*****************************************")
                print("\nSee {} in {}".format(f,stored_output_dir))
                raise Exception("{} failed!".format(f))

def test_ensembles():

    ensembles = get_test_ensembles()
    for ens_info in ensembles:
        check_gcmrun_shas(ens_info)
        check_gcmrun_files(ens_info)


if __name__ == '__main__':
    test_ensembles()
