Contributing to CanESM (Developers guide)
=========================================

For a quickstart on modifying and contributing back to ``CanESM``, readers
are directed to the :ref:`modifying CanESM quickstart guide <Modifying CanESM>`. 
For more comprehensive documentation on the version control workflow employed
within ``CanESM``, readers should refer to the 
:ref:`CanESM version control guide.<CanESM Version Control System>`

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   quickstart/quickstart_developing
   canesm-vcs/canesm-vcs
   
   


