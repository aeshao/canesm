Running CanESM on ECCC HPC systems
==================================

This document was adapated from the contents of the "Running_readme.md"
documenting in the ``CanESM`` repository.


Introduction
------------

This document describes the five basic steps required to run
``CanESM``/``CanAM`` from Version Control. If this is your first run, see the
section "One time setup" :ref:`below <One Time Setup>`. For instructions on how
to modify the code see :ref:`the developers guide <Contributing to CanESM
(Developers guide)>`. 

Runid Guidance
^^^^^^^^^^^^^^
 
 As part creating a run, users must select a "runid", or run identifier, that
 will be used to differeniate their runs from others. In *general* the chosen
 runids are free form, but there are *some* restrictions, specifically, they
 must contain **only lower case alphanumeric characters [a-z] and [0-9], the
 hyphen "-" and the period "."**

Setting up a run 
----------------

These steps are mostly the same for ``CanESM`` and ``CanAM`` ``AMIP`` runs,
with differences noted below. Setup is done on banting or daley.

1. **Call** ``setup-canesm``

    In the directory where you want to setup the run (normally
    ``~/PRODUCTION/runid``), call ``setup-canesm``, specifying at a minimum the
    runid and version of the code to use, e.g. (CHANGE at least runid and
    ver!):

    AMIP:

    .. code-block:: text

        setup-canesm  ver=ABC config=AMIP runid=XXX

    Coupled (ESM):

    .. code-block:: text

        setup-canesm  ver=ABC config=ESM runid=XXX

    where ``ver`` is a tag/commit/SHA1 checksum from the ``CanESM`` "super
    repo" (see Model versions section :ref:`below <Model Versions>`, or you
    could use the branch name such as ``develop_canesm``);
    ``config=AMIP`` (default) OR ``config=ESM`` for a coupled run 
    and ``runid`` is the unique runid. 

    .. note:: 

        - If you not have access to `setup_canesm`, see the :ref:`one time
          setup documentation below <One Time Setup>`.
        - If you want to setup the run from a user fork, you will need to provide
          the ``repo=`` argument. See information 
          :ref:`here <(Internal Use Only) Launching runs from your fork>` for more info.
        - **If you plan on doing development using the code cloned for this run**, 
          be sure to refer to 
          :ref:`the quickstart guide on modifying CanESM <Modifying CanESM>` noting
          that the CanESM source code has been cloned into ``CanESM_source_link``
          within the setup directory. 

    .. warning::

        **Do not** re-use runids of existing runs, even if setting up from a different
        account! 

2. **Source the run-time environment**

    After running ``setup-canesm``, you will see output like:

    .. code-block:: text

        Setup the interactive CCCma environment (now) by executing the following line:

            . env_setup_file

    Follow these directions to get the proper run-time environment.

    .. note:: 

        If you log out, you will need to source this file again when you return.

3. **Create the job string**

    Edit the file ``canesm.cfg``, to set dates and run options, as documented in
    that file, and then create the job string, which is used to run the job, based off your
    configuration settings and is submitted to start the run.

    .. code-block:: text

        vi canesm.cfg
        ...              # Change start and end dates, etc. runid has been set already by setup-canesm

        ./make_job_XXX   # Create the job string by executing the make_job

4. **Compile the executables**

    Executables are compiled interactively by the user. ``setup-canesm`` will
    have extracted a file called ``compile_XXX`` (where ``XXX`` is the runid).
    Execute the compile script to do the compilation, specifying the model job
    as the first command line argument:

    .. code-block:: text

        ./compile_XXX XXX_2652m01_2656m12_job
    
    The compilation will take a few minutes. All executables are now compiled
    in the source trees, but links to them will be created at 
    ``$CCRNSRC/executables``. See the notes 
    :ref:`below <Tips for Compiling>` if your compilation is failing.

    Note that the AGCM merged diagnostic deck is now created during the
    compilation step by executing the ``make_merged_diag_deck`` script.  The
    output merged diagnostic deck ``merged_diag_deck.dk`` is now placed in
    ``$CCRNSRC/bin/merged_diag_deck.dk``, outside of the CanESM5 repository.

4. **Save restarts to RUNPATH**

    ``setup-canesm`` will extract the script ``save_restart_files_XXX``, which
    you can execute to save the default restarts (or modify as appropriate to
    save restarts from another run on disk for use in the current run).

    .. code-block:: text

        save_restart_files_XXX XXX_2652m01_2656m12_job

    The ``save_restart_files*`` script contains a hacker job to reset the count
    value in the restart file to correspond to model year.  Alternatively, use
    the ``tapeload_rs`` script to load restart files for a given runid/year off
    tape. The jobstring is provided as an argument.


5. **Submit the job.**

    .. code-block:: text

        rsub MACH $MODEL_JOB

    where ``MACH`` is currently either ``daley`` or ``banting``. If you want to
    submit the job to be submitted automatically after compilation finishes, do
    the following:


    .. code-block:: text

        ./compile_XXX XXX_2652m01_2656m12_job ; ./save_restart_files_XXX ; rsub brooks XXX_2652m01_2656m12_job
        
    .. note:: 
        
        ``rsub`` should become availabe once you've source the run-time environment

Continuing an existing run
--------------------------

Run has crashed before its scheduled end
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If a run crashes, the normal recovery procedure is to try and resubmit the
model jobstring from the ``.crawork`` directory, i.e.:

    .. code-blocK:: text 

        cd ~/.crawork
        ls -lrt | grep $runid # look for a file containing modljob
        rsub MACH JOB
    
where ``MACH`` is banting or daley (as indicated in the job name) and ``JOB``
is the jobstring identified on the second line.

Run has reached the end of it's jobstring
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  To continue a run which has run out of string, modify the dates in the
  ``canesm.cfg`` file, create a new jobstring and submit this.

  .. warning::
  
      One thing to be aware of is that you must ensure you are in the correct
      environment for your runid. That is, you should source the
      ``env_setup_file`` for your run   (as in step 1 above). A quick check is to
      do ``which rsub``.
  
Model versions
--------------

The branch ``develop_canesm`` is used to integrate in new changes, and always
reflects the lastest developments. We strive to keep ``develop_canesm`` stable
and even bit-identical to the previous tagged release, but issues can arise.

When important changes occur, a tagged release is issued. Tagged releases are
thoroughly tested, stable versions of the model (although old taggaed releases
might not function as HPC changes). The latest tagged release should always be
functional and stable, and represents a reliable starting point for work.
    
To find the latest tagged release, visit `CanESM5 repository on gitlab
<https://gitlab.science.gc.ca/CanESM/CanESM5>`_.  From the top horizontal menu
bar, select 'repository'. Then from the secondary menu bar, select 'Tags'. The
latest tagged release is listed at the top, with its commit number underneath.
Use that for ``ver=`` in the call to ``setup-canesm``.

When new tagged releases are issued, users should merge these into their
working branches ASAP.

One time setup
--------------

You do require access to one script (``setup-canesm``). To get this modify
``~/.profile`` to include the line:

    .. code-block:: text

        export PATH=/home/scrd101/canesm_bin_latest:$PATH

(and make sure to log back in or to source ``~/.profile``). It is suggested that
every time you use an account to start or continue a run, you first verify that
``~/.profile`` is clean.

You must also make sure that the account that you are using has its SSH keys
added to gitlab. If you get prompted for a password during cloning/setup then
this has not been done. Follow
`these <https://wiki.cmc.ec.gc.ca/wiki/Subscribing_To_Gitlab>`_ instructions to
add your keys.

Conflicts in your environment might cause problems. If things are not working
as expected, try again with a "clean" environment. By clean we mean that
``~/.profile`` does not source the CCCma environment (and preferably, should be
nearly empty). The line

    .. code-block:: text 

        . /home/scrd101/generic/sc_cccma_setup_profile

which appears by default in CCCma user accounts should be commented out to make
the environment clean (and log back out/in again).

Note on setup-canesm
--------------------

Your call to ``setup-canesm`` will:

  - clone the source code (for ver) to a place called ``RUNID_ROOT``, in ``/home/ords/crd/ccrn/$USER/$runid``.
  - extract a reference ``canesm.cfg`` for the user to edit as they need (for both the model and diagnostics).
  - extract some required tools like ``make_job_${runid}``, ``save_restart_files_${runid}``, and
    ``tapeload_rs_${runid}`` along with other helper scripts that users may or may not use.
  - extract a compilation script for the user to compile the source code.
  - create soft link to the ``CanESM`` source code in ``RUNID_ROOT`` (``CanESM_source_link``)
  - append an entry to ``.setup-canesm.log``, detailing what you have done.

For more help and options see:

    .. code-block:: text 

        setup-canesm -h

It must again be stated that you should source the ``env_setup_file`` as
instructed by ``setup-canesm`` to get an interactive environment:

    .. code-block:: text 

        . env_setup_file

If you ever log-off or start up a new terminal, you need to be sure to source
this file. This may seem onerous, but it is being done to try and ensure that
each run in self-contained and repeatable.


    .. note:: 

        ``setup-canesm`` will optionally compile, save restarts and submit the
        job for you (see options -a, -c)

    .. warning::
    
        you can specify a branch as a `ver`. This is somewhat dangerous to reproducibility, because branches change in time.
        By using a SHA1 checksum, you are guaranteed to be able to use the exact same code in the future.

Tips for compiling
------------------
If any of the compilations fail (meaning the executables can't be found in
``$CCRNSRC/executables``), you should inspect the errors in the hidden
``.compile-canesm*`` logs that will appear in the run directory.

All of the components now use an in-source compilation system, so once you 
find the problem, you can go into the source code and make the necessary modifications. 
Once done, you can recompile by simply calling the ``compile-${runid}`` script again. 
It should also be noted that if you only want to recompile a single component, you can
utilize various flags to limit the compilation - specifically, the compile script has 
the following interface:

    .. code-block:: text

        Usage:

          compile-canesm.sh [-d] [-a] [-c] [-n] [-f] MODEL_JOB

             -d compile CanDIAG
             -a compile CanAM
             -c compile CanCPL
             -n compile CanNEMO
             -f force a clean compilation

          If none of the flags are specified, compile all components reusing files that
          may have already been generated.

          MODEL_JOB job must be specified and exist.

Users should be aware that the compile script:

1. doesn't trigger a *full* recompile by default, to trigger a full recompile, use the ``-f`` option.
2. the compile script also generates two ``cpp`` files, depending on your configuration settings (``cppdef_config.h``,
   and ``cppdef_sizes.h``). You should clean these files if you want your new settings to be applied properly, 
   or use the ``-f`` option, which also cleans them.

